<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Signin</title>
    <!-- Bootstrap core CSS -->
    {{ HTML::style('css/bootstrap.min.css')}}
    {{ HTML::style('css/default.css')}}
    {{ HTML::style('css/signin.css')}}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    @if ($errors->has())
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <p class="text-center">Something went wrong!</p>
            @foreach ($errors->all() as $error)
                <p class="text-center">{{ $error }}</p>
            @endforeach
            <p class="text-center">Try again!</p>
        </div>
    @endif

    @if (Session::has('failure'))
        <div class="alert alert-success alert-dismissible center-block" role="alert">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <p class="text-center">{{ Session::get('failure') }}</p>
        </div>
    @endif

    <div class="container">
        <form class="form-signin" role="form" action="/users/login" method="POST">
            <h2 class="form-signin-heading text-center">Welcome back</h2>
            <label for="email" class="sr-only">Email address</label>
            <input type="email" id="email" name="email" class="form-control" placeholder="Email address" required="" autofocus="">
            <label for="password" class="sr-only">Password</label>
            <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="">
            <div class="checkbox">
                <!-- (NOT IMPLEMENTED YET) <label>
                    <input type="checkbox" value="remember-me">Remember me
                </label> -->
                <a href="/" role="button" class="btn btn-primary btn-me pull-right">Back</a>
            </div>
            <button class="btn btn-lg btn-success btn-block" type="submit">Sign in</button>
        </form>
    </div> <!-- /container -->
    {{ HTML::script('js/jquery-1.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
</body>
</html>