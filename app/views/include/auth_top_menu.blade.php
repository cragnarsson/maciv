<hr>
<p> Welcome back {{ Auth::user()->email}}!</p>
<nav>
	<a href="/users/logout" class="btn btn-info" role="button">LOGOUT</a>
	<hr>
	<h2>timeline settings</h2>
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<button class="btn btn-success dropdown-toggle" type="button" data-toggle="collapse" data-parent="#accordion" href="#serviceSelection" aria-expanded="true" aria-controls="serviceSelection">
			Add service
			<span class="caret"></span>
		</button>
	</div>
	<div id="serviceSelection" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
		<p>Select the service you want to gather data from.</p>
		<p>You will be required to login to your account at the selected service.</p>
		@if (Auth::user()->isInstagramAdded())
			<?php echo "<a href='" . Instagram::getLoginUrl() . "' class='btn btn-info btn-lg disabled' role='button'>Instagram</a>"; ?>
		@else 
			<?php echo "<a href='" . Instagram::getLoginUrl() . "' class='btn btn-info btn-lg' role='button'>Instagram</a>"; ?>
		@endif
		@if (Auth::user()->isTwitterAdded())
			<a href="/authentication/twitter-redirect" class="btn btn-info btn-lg disabled" role="button">Twitter</a>
		@else 
			<a href="/authentication/twitter-redirect" class="btn btn-info btn-lg" role="button">Twitter</a>
		@endif
	</div>
	@if (Auth::user()->has_timeline)
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<button type="button" class="btn btn-success" data-toggle="collapse" data-target="#sort" aria-expanded="false" aria-controls="sort">
				filter timeline
				<span class="caret"></span>
			</button>
		</div>
		<div id="sort" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
			<p>Collect data from:</p>
			{{ Form::open(array('url' => '/timeline/update')) }}
				<fieldset>
				@if (Auth::user()->isInstagramAdded())
					<label class="checkbox-inline">
						@if (Auth::user()->isInstagramUsedForTimeline())
							{{ Form::checkbox('instagram', 'Instagram', true) }} Instagram
						@else
							{{ Form::checkbox('instagram', 'Instagram', false) }} Instagram
						@endif
					</label>
				@endif
				@if (Auth::user()->isTwitterAdded())
					<label class="checkbox-inline">
						@if (Auth::user()->isTwitterUsedForTimeline())
							{{ Form::checkbox('twitter', 'Twitter', true) }} Twitter
						@else
							{{ Form::checkbox('twitter', 'Twitter', false) }} Twitter
						@endif
					</label>
				@endif
				</fieldset>
				{{ Form::submit('UPDATE TIMELINE', ['class' => 'btn btn-success']) }}
			{{ Form::close() }}
		</div>
	@endif
</nav>
<hr>