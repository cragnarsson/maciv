<div class="alert alert-success alert-dismissible center-block" role="alert">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<p class="text-center">Cool! You have successfully added
		{{ Session::get('service_added') }}!</strong></p>
</div>