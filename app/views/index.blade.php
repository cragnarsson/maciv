@section('title')
MACIV.IO
@stop

@section('content')
    @if (Session::has('service_added'))
        @include("include/auth_service_added")
    @endif
    @if (Session::has('filter_error'))
    	@include("include/auth_filter_error")
    @endif
	@if (Auth::check())
		@include("include/auth_top_menu")
		@if (Auth::user()->has_timeline)
			@include("include/auth_timeline")
		@else 
			@if(Auth::user()->isServiceAdded())
				@include("include/auth_no_timeline")
			@else
				@include("include/auth_no_service")
			@endif

		@endif
	@else
		@include('include/no_auth_index')
	@endif
@stop


@section('scripts')
@stop
