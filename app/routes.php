<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Index page
|--------------------------------------------------------------------------
|
*/
Route::get('/', 'HomeController@index');

/*
|--------------------------------------------------------------------------
| User actions (Implicit Controller)
|--------------------------------------------------------------------------
|
*/
Route::controller('users', 'UserController');

/*
|--------------------------------------------------------------------------
| Authentication actions (Implicit Controller)
|--------------------------------------------------------------------------
|
*/
Route::controller('authentication', 'AuthenticationController');

/*
|--------------------------------------------------------------------------
| Timeline actions (Implicit Controller)
|--------------------------------------------------------------------------
|
*/
Route::controller('timeline', 'TimelineController');