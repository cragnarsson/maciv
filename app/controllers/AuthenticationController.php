<?php

class AuthenticationController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| AuthenticationController
	|--------------------------------------------------------------------------
	|
	| This controller is used to handle authentication towards the services we collect data from.
	| Current actions: 
	|--------------------------------------------------------------------------
	| 	instagram - GET - The callback URL for Instagram.
	|	twitterRedirect - GET - The URL used to redirect to twitter.
	|	twitter - GET - The callback URL for Twitter.
	|
	*/

/*
|--------------------------------------------------------------------------
| instagram - GET - The callback URL for Instagram.
|--------------------------------------------------------------------------
|
*/
	public function getInstagram()
	{
		// Get OAuthToken
    	$data = Instagram::getOAuthToken(Input::get('code'));
    	// Save the user id and token in DB
		$user = Auth::user();
		$user->instagram_added = true;
		$user->instagram_id = $data->user->id;
		$user->instagram_access_token = Crypt::encrypt($data->access_token);
		$user->has_added_service = true;
		$user->save();

    	// Redirect to authorized view        
    	return Redirect::to('/')->with('service_added', "Instagram");
	}

/*
|--------------------------------------------------------------------------
| twitter-redirect - GET - The URL used to redirect to twitter.
|--------------------------------------------------------------------------
|
*/
	public function getTwitterRedirect()
	{
		// Reqest tokens
    	$tokens = Twitter::oAuthRequestToken();
	    // Redirect to twitter
	    Twitter::oAuthAuthenticate(array_get($tokens, 'oauth_token'));
	    exit;
	}
/*
|--------------------------------------------------------------------------
| twitter - GET - The callback URL for Twitter.
|--------------------------------------------------------------------------
|
*/
	public function getTwitter()
	{
		// Oauth token
	    $token = Input::get('oauth_token');
	    // Verifier token
	    $verifier = Input::get('oauth_verifier');
	    // Request access token
	    $accessToken = Twitter::oAuthAccessToken($token, $verifier);
	    // Save tokens and user ID in DB
	    $user = Auth::user();
	    $user->twitter_added = true;
	    $user->twitter_access_token = Crypt::encrypt($accessToken['oauth_token']);
	    $user->twitter_access_token_secret = Crypt::encrypt($accessToken['oauth_token_secret']);
	    $user->twitter_id = $accessToken['user_id'];
	    $user->has_added_service = true;
	    $user->save();

		// Redirect to authorized view        
    	return Redirect::to('/')->with('service_added', "Twitter");

	}

}