<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	protected $layout =  'master';

	public function index()
	{	
		if (Auth::check()) {
			if (Auth::user()->isEverythingUsedForTimeline()) { 
				// If everything is used for the timeline we get from both Instagram and Twitter
				$media = Auth::user()->getMedia();
				$tweets = Auth::user()->getTweets();
				$merged = array_merge($media, $tweets);
				usort($merged, function($a1, $a2) {
				   $v1 = strtotime($a1['created_time']);
				   $v2 = strtotime($a2['created_time']);
				   return $v2 - $v1;
				});
				$data["content"] = json_decode(json_encode($merged));
			} else if(Auth::user()->isInstagramUsedForTimeline()) {
				// If only Instagram is used we only get Instagram media
				$data["content"] = json_decode(json_encode(Auth::user()->getMedia()));
			} else {
				// If only Twitter is used we only get Twitter 
				$data["content"] = json_decode(json_encode(Auth::user()->getTweets()));
			}
			$this->layout->content = View::make('index', $data);
		} else {
			$this->layout->content = View::make('index');
		}
	}

}
