<?php

class UserController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| UserController
	|--------------------------------------------------------------------------
	|
	| This controller is used to handle user related actions.
	| Current actions: 
	|--------------------------------------------------------------------------
	| 	store - POST - Handles sign-up of new users.
	|	login - POST - Handles login of a user.
	|	logout - GET - Handles logout of a user.	
	|
	*/

/*
|--------------------------------------------------------------------------
| store - POST - Handles sign-up of new users.
|--------------------------------------------------------------------------
|
*/
	public function postStore()
	{
		$input = Input::all();

		$rules = array( 
			'email' => 'required|email|Unique:users|',
			'password' => 'required|min:6',
			);

		$validator = Validator::make($input, $rules);

		if ($validator->fails()) {
			return Redirect::to('/')->withErrors($validator)->withInput();
		}
		else {
			$user = new User;
    		$user->email = Input::get('email');
    		$user->password = Hash::make(Input::get('password'));
    		$user->save();
			if($user){
           		Auth::login($user);
           		return Redirect::to('/');
        	}
		}
	}

/*
|--------------------------------------------------------------------------
| login - POST - Handles login of a user.
|--------------------------------------------------------------------------
|
*/
	public function postLogin()
	{
		// Define rules for validator
		$rules = array(
			'email'    => 'required|email', 
			'password' => 'required' 
		);

		// Run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules);

		// If the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('login')
				->withErrors($validator) // Send back all errors to the login form
				->withInput(Input::except('password')); // Send back the input (not the password) so that we can repopulate the form
		} else {

			// Create our user data for the authentication
			$userdata = array(
				'email' 	=> Input::get('email'),
				'password' 	=> Input::get('password')
			);
			// Attempt to do the login
			if (Auth::attempt($userdata)) {
				// Validation successful
				return Redirect::to('/');
			} else {
				// Validation not successful, send back to form	
				$flashmessage = '<strong>Wrong password or mail.</strong> Try again.';
				return Redirect::intended('/users/login')->with('failure', $flashmessage);
			}
		}
	}

/*
|--------------------------------------------------------------------------
| login - GET - Used to get the login view
|--------------------------------------------------------------------------
|
*/
	public function getLogin()
	{
		if (Auth::check()) {
			// If the user is already authenticated, send them to index
			return Redirect::to('/');
		}
		else {
			// else, return the login view
			return View::make('login');
		}
	}

/*
|--------------------------------------------------------------------------
| logout - GET - Handles logout of a user.
|--------------------------------------------------------------------------
|
*/
	public function getLogout()
	{
		Auth::logout();
		$flashmessage = '<strong>Bye!</strong> You have now logged out.';
		return Redirect::to('/')->with('success', $flashmessage);
	}
}