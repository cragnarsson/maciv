<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function($table)
		{
			$table->increments('id');
			$table->string('email');
			$table->string('password');
			$table->string('remember_token');
			$table->boolean('has_timeline');
			$table->boolean('has_added_service');
			$table->boolean('instagram_added');
			$table->boolean('instagram_used_for_timeline');
			$table->boolean('instagram_media_received');
			$table->text('instagram_access_token');
			$table->integer('instagram_id');
			$table->boolean('twitter_added');
			$table->boolean('twitter_used_for_timeline');
			$table->boolean('twitter_tweets_received');
			$table->text('twitter_access_token');
			$table->text('twitter_access_token_secret');
			$table->text('twitter_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
