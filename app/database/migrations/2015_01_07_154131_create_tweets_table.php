<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTweetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tweets', function($table)
		{
			$table->increments('id');
			$table->integer('owner');
			$table->string('created_time');
			$table->string('location_name');
			$table->decimal('latitude', 7, 5);
			$table->decimal('longitude', 7, 5);	
			$table->integer('retweet_count');	
			$table->integer('favorite_count');
			$table->string('media_url_https');
			$table->string('profile_image_url_https');
			$table->string('caption_text');
			$table->bigInteger('image_id');	
			$table->string('source');
			$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tweets');
	}

}
