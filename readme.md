Welcome to MACIV 
==============================

What is MACIV?
-------------
MACIV is a mash-up web application that collects data from social networks and visualizes the data in an attractive timeline. 

MACIV is developed by students as a university project.

Technologies used
------------- 
PHP (Laravel)
MySQL
Bootstrap
jQuery
Timeline.js

MACIV - Timeline of memories